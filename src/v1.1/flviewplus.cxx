// generated by Fast Light User Interface Designer (fluid) version 1.0304

#include "flviewplus.h"
#include <stdio.h>
#include <stdlib.h>
#include "ccode.h"
static char user_arg1[PATH_MAX]; 
static int flfontsize = 14; 

void loadpanel( const char *pattern ) {
  printf("Load Panel!\n");
  
  
    
    int fetchi;
    FILE *fp5;
    FILE *fp6;
    char fetchline[PATH_MAX];
    char fetchlinetmp[PATH_MAX];
  
      fbrow->clear( );    
      
      fp6 = fopen( pattern , "rb");
      while( !feof(fp6) ) 
      {
            fgets(fetchlinetmp, PATH_MAX, fp6); 
            strncpy( fetchline, "" , PATH_MAX );
            for( fetchi = 0 ; ( fetchi <= strlen( fetchlinetmp ) ); fetchi++ )
              if ( fetchlinetmp[ fetchi ] != '\n' )
                   fetchline[fetchi]=fetchlinetmp[fetchi];
                   
                  if ( !feof( fp6 ) ) 
                  {
                   fbrow->add( fetchline );    
                  }
  
       }
       fclose( fp6 );
}

Fl_Double_Window *win1=(Fl_Double_Window *)0;

static void cb_Reload(Fl_Button*, void*) {
  loadpanel( user_arg1 );
}

Fl_Browser *fbrow=(Fl_Browser *)0;

static void cb_fbrow(Fl_Browser*, void*) {
  // statusbar

char charo[PATH_MAX];

        
if ( fbrow->value() >= 1 ) 
{
   // Adds and view the line 
   
   //  statusbar->value(     fbrow->text( fbrow->value() )      );
    snprintf( charo , sizeof( charo ), "L%d: %s" ,  fbrow->value()  ,     fbrow->text( fbrow->value() )   );
    statusbar->value( charo );
};
}

static void cb_Cancel(Fl_Button*, void*) {
  exit( 0 );
}

static void cb_Font(Fl_Button*, void*) {
  flfontsize++;
   fbrow->labelsize( flfontsize  );
   fbrow->textsize( flfontsize  );


//loadpanel( user_arg1 );
}

static void cb_Font1(Fl_Button*, void*) {
  flfontsize--;

fbrow->labelsize( flfontsize  );
   fbrow->textsize( flfontsize  );


//loadpanel( user_arg1 );
}

static void cb_Edit(Fl_Button*, void*) {
  char foocharo[PATH_MAX];
        
        snprintf( foocharo , sizeof( foocharo ), " screen -d -m  fledit \"%s\"  " ,   user_arg1   ); 
        
        printf( " Command %s\n", foocharo ); 
        system(   foocharo ); 
        
           

  loadpanel(   user_arg1   );
}

Fl_Output *statusbar=(Fl_Output *)0;

Fl_Double_Window* make_window() {
  { win1 = new Fl_Double_Window(445, 290, "FLVIEWPLUS");
    { Fl_Button* o = new Fl_Button(10, 235, 105, 25, "&Reload");
      o->labelfont(1);
      o->callback((Fl_Callback*)cb_Reload);
    } // Fl_Button* o
    { fbrow = new Fl_Browser(10, 45, 425, 180);
      fbrow->callback((Fl_Callback*)cb_fbrow);
      Fl_Group::current()->resizable(fbrow);
      fbrow->labelsize( flfontsize  );
      fbrow->textsize( flfontsize  );
      fbrow->type(FL_HOLD_BROWSER);
    } // Fl_Browser* fbrow
    { Fl_Button* o = new Fl_Button(345, 235, 90, 25, "&Cancel");
      o->labelfont(1);
      o->callback((Fl_Callback*)cb_Cancel);
    } // Fl_Button* o
    { Fl_Button* o = new Fl_Button(200, 235, 75, 25, "Font (&+)");
      o->labelfont(1);
      o->callback((Fl_Callback*)cb_Font);
    } // Fl_Button* o
    { Fl_Button* o = new Fl_Button(120, 235, 75, 25, "Font (&-)");
      o->labelfont(1);
      o->callback((Fl_Callback*)cb_Font1);
    } // Fl_Button* o
    { Fl_Box* o = new Fl_Box(10, 5, 425, 30, "File View+");
      o->box(FL_ENGRAVED_BOX);
      o->labeltype(FL_ENGRAVED_LABEL);
    } // Fl_Box* o
    { Fl_Button* o = new Fl_Button(280, 235, 60, 25, "Edit");
      o->labelfont(1);
      o->callback((Fl_Callback*)cb_Edit);
    } // Fl_Button* o
    { statusbar = new Fl_Output(10, 265, 425, 20);
      statusbar->color(FL_BACKGROUND_COLOR);
      statusbar->labelsize(12);
      statusbar->textsize(12);
    } // Fl_Output* statusbar
    win1->end();
  } // Fl_Double_Window* win1
  return win1;
}

int main( int argc, char *argv[]) {
  printf( " == FLVIEW == \n" );
  
    char mydirnow[2500];
    printf( "Current Directory: %s \n", getcwd( mydirnow, 2500 ) );
   
    char filein[PATH_MAX];
    char filesource[PATH_MAX];
    strncpy( filesource , "/etc/hostname" , PATH_MAX );
    strncpy( filein ,     filesource , PATH_MAX );
    if ( argc == 2)
    if ( strcmp( argv[1] , "" ) !=  0 ) 
    {
            strncpy( filein, argv[ 1 ], PATH_MAX );
            strncpy( filesource , argv[ 1 ], PATH_MAX );
    }
    strncpy( user_arg1, filein , PATH_MAX );
   
    make_window();
    
    loadpanel(   filein   );
    
    win1->show();
    Fl::run();
}
