# fltvstream

TV Stream Tuner

(Library: FLTK 1.3 
/ https://www.fltk.org/pub/fltk/1.3.5/fltk-1.3.5-source.tar.gz)

![](media/fltvstream.png)


GNU Free Software for Linux, FreeBSD, OpenBSD, and NetBSD. 


## Getting started

FLTVSTREAM is a FLTK project to use view web streams, by using VLC (default).


````
  /usr/local/bin/fltvstream 
````







## Installation 

1.) FLTK and C++

````
 apt-get update 
 apt-get install ffmpeg mpv vlc screen xterm libfltk1.3-dev g++ make 
 mkdir /usr/local/bin/
 c++ -lm   src/fltvstream.cxx  -lfltk -lX11     -o  /usr/local/bin/fltvstream
 c++ -lm   src/flviewplus.cxx  -lfltk -lX11     -o  /usr/local/bin/flviewplus
 c++ -lm   src/flview.cxx  -lfltk -lX11     -o  /usr/local/bin/flview
````

2.) "youtube-dl"

The installation of "youtube-dl" is required. 



## URL/Stream Browser

![](media/1653358064-1-fox-news-fltvstream-raspberry-pi-rpi3b+.png)


## Custom Playlist

````
  mkdir ~/.fltvstream  ;  notepad ~/.fltvstream/playlist.txt
````

**Format:**

Author of Song/Album - Title of Album (Year);URL


## Capture a Stream/Tube/Video

In the tab "Channel", there are several options.
One of them is to use wget to fetch a tube (using youtube-dl and wget).
Wget needs to be installed. 




## Settings 

![](media/1655057143-screenshot.png)
![](media/1655057151-screenshot.png)
![](media/1655057159-screenshot.png)




Have Fun !





